//
//  Date.swift
//  cbFramework
//
//  Created by Doruk Kaan Bolu on 7.09.2023.
//  Copyright © 2023 codebehind. All rights reserved.
//

import Foundation

extension Date {
    public func toString(dateFormat format: String) -> String {

        let locale = Locale.init(identifier: cbFramework.shared.context?.langId == .turkish ? "tr_TR" : "en_US")

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = locale as Locale
        return dateFormatter.string(from: self)
    }
}
