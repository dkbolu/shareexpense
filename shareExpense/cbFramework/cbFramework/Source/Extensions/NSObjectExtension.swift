//
//  NSObjectExtension.swift
//  cbframework
//
//  Created by Doruk Kaan Bolu on 1.11.2021.
//

import UIKit

public extension NSObject {
    func po() {
        debugLog("\(self)")
    }
}

private func debugLog(_ message: @autoclosure () -> String) {
    if cbFramework.shared.context?.debugMod ?? false {
        print("\(message())")
    }
}

