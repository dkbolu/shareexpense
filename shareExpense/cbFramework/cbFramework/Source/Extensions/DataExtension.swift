//
//  DataExtension.swift
//  cbframework
//
//  Created by Doruk Kaan Bolu on 1.11.2021.
//

import Foundation

public extension Data {
    var prettyJSONString: String {
        guard let object = try? JSONSerialization.jsonObject(with: self, options: []),
              let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]),
              let prettyPrintedString = String(data: data, encoding: .utf8) else { return "" }
        
        return prettyPrintedString
    }
}
