//
//  UserDefaultsManager.swift
//  cbframework
//
//  Created by Doruk Kaan Bolu on 1.11.2021.
//

import Foundation

public class UserDefaultsManager: NSObject {

    public static let shared = UserDefaultsManager()

    // MARK: String
    public func writeString(string: String, key: String) {
        UserDefaults.standard.set(string, forKey: key)
    }

    public func readString(key: String) -> String {
        let string = UserDefaults.standard.string(forKey: key) ?? ""
        return string
    }

    // MARK: Int
    public func writeInteger(integer: NSInteger, key: String) {
        UserDefaults.standard.set(integer, forKey: key)
    }

    public func readInteger(key: String) -> Int {
        let integer = UserDefaults.standard.integer(forKey: key)
        return integer
    }

    // MARK: Bool
    public func writeBool(bool: Bool, key: String) {
        UserDefaults.standard.set(bool, forKey: key)
    }

    public func readBool(key: String) -> Bool {
        let bool = UserDefaults.standard.bool(forKey: key)
        return bool
    }

    // MARK: - Object
    public func writeObject<T: Codable>(object: T, key: String) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(object) {
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: key)
        }
    }

    public func readObject<T: Codable>(type: T.Type, key: String) -> T? {
        if let object = UserDefaults.standard.object(forKey: key) as? Data {
            let decoder = JSONDecoder()
            do {
                if let returnObject = try decoder.decode(T?.self, from: object) {
                    return returnObject
                }
            } catch {
                error.localizedDescription.po()
            }
        }
        return nil
    }

    // MARK: NSNumber Sequence
    public func writeSequence(sequence: [NSNumber], key: String) {
        UserDefaults.standard.set(sequence, forKey: key)
    }

    public func readSequence(key: String) -> [NSNumber] {
        let seq = UserDefaults.standard.array(forKey: key)
        return (seq ?? []) as? [NSNumber] ?? []
    }

    // MARK: Remove
    public func removeAllKeys() {
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        UserDefaults.standard.synchronize()
    }

    public func removeObject(key: String) {
        UserDefaults.standard.removeObject(forKey: key)
    }
}
