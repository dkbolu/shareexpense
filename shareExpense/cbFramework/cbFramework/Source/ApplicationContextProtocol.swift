//
//  ApplicationContextProtocol.swift
//  cbFramework
//
//  Created by Doruk Kaan Bolu on 14.08.2022.
//  Copyright © 2022 codebehind. All rights reserved.
//

import Foundation
public protocol ApplicationContextProtocol: AnyObject {
    var debugMod: Bool { get set }
    var langId: Language { get set }
}

public class cbFramework: NSObject {
    static let shared = cbFramework()
    internal weak var context: ApplicationContextProtocol?

    public static func setup(context: ApplicationContextProtocol) {
        cbFramework.shared.context = context
    }
}

public enum Language: String {
    case turkish = "tr"
    case english = "en"
}
