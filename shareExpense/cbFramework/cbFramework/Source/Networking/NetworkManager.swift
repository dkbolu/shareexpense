//
//  NetworkManager.swift
//  cbframework
//
//  Created by Doruk Kaan Bolu on 1.11.2021.
//

import Moya
import Foundation

enum StatusCodes: Int {
    case ok = 200
    case badRequest = 400
    case unauthorized = 401
    case forbidden = 403
}

//enum CBError: Error {
//    case runtimeError(Result)
//}

class NetworkManager: NSObject {

    func request<T: TargetType,
                 R: Decodable>(showIndicator: Bool = true, endpoint: T,
                               completion: @escaping (R?, Error?) -> Void) {
        let plugins = [PluginType]()
        let provider = MoyaProvider<T>(plugins: plugins)
        _ = call(showIndicator: showIndicator, endpoint: endpoint, provider: provider, completion: completion)
    }

    fileprivate func call<T: TargetType, R: Decodable>(showIndicator: Bool, endpoint: T,
                                                       provider: MoyaProvider<T>,
                                                       completion: @escaping (R?, Error?) -> Void) -> Cancellable {
        return provider.request(endpoint) { result in
            switch result {

            case let .success(response):
                "\n\n-----> RESPONSE: (\(endpoint.path)) Status Code: \(response.statusCode)".po()
                let decoder = JSONDecoder()
                do {
                    _ = try response.filterSuccessfulStatusCodes()
                    "\(response.data.prettyJSONString)".po()
                    let responseModel = try decoder.decode(R?.self, from: response.data)
                    completion(responseModel, nil)
                } catch {
                    error.localizedDescription.po()
                    completion(nil, error)
                }
            case let .failure(error):
                error.errorDescription?.po()
                completion(nil, error)
            }
        }
    }
}
