import ProjectDescription

let project = Project(
    name: "cbFramework",
    organizationName: "codebehind",
    settings: nil,
    targets: [
        Target(
            name: "cbFramework",
            platform: .iOS,
            product: .framework,
            bundleId: "com.codebehind.cbFramework",
            infoPlist: "cbFramework/Info.plist",
            sources: ["cbFramework/Source/**"],
            dependencies: [
                .package(product: "Moya")
            ]),
    ])

