import ProjectDescription

let project = Project(
    name: "ShareExpense",
    organizationName: "codebehind",
    packages: [
        Package.remote(
            url: "https://github.com/Moya/Moya.git",
            requirement: .upToNextMajor(
                from: Version(15, 0, 0)))
        ,
        Package.remote(
            url: "https://github.com/firebase/firebase-ios-sdk",
            requirement: .upToNextMajor(from: Version(9, 0, 0)))
    ],
    settings: nil,
    targets: [
        Target(
            name: "ShareExpense",
            platform: .iOS,
            product: .app,
            bundleId: "com.codebehind.shareExpense",
            infoPlist: "shareExpense/Info.plist",
            sources: ["shareExpense/Source/**"],
            resources: ["shareExpense/Resource/**"],
            dependencies: [
                .project(target: "cbFramework", path: .relativeToManifest("cbFramework")),
                .project(target: "cbComponents", path: .relativeToManifest("cbComponents")),
                .package(product: "FirebaseAuth"),
                .package(product: "FirebaseAnalytics"),
                .package(product: "FirebaseCrashlytics"),
                .package(product: "FirebaseFirestore")
            ],
            settings: nil),
        Target(
            name: "ShareExpenseTest",
            platform: .iOS,
            product: .unitTests,
            bundleId: "com.codebehind.shareExpenseTest",
            infoPlist: "shareExpenseTests/Info.plist",
            sources: ["shareExpenseTests/Source/**"],
            dependencies: [
                .target(name: "ShareExpense")
            ])
    ])
