import ProjectDescription

let project = Project(
    name: "cbComponents",
    organizationName: "codebehind",
    settings: nil,
    targets: [
        Target(
            name: "cbComponents",
            platform: .iOS,
            product: .framework,
            bundleId: "com.codebehind.cbComponents",
            infoPlist: "cbComponents/Info.plist",
            sources: ["cbComponents/Source/**"]
        )
    ]
)

