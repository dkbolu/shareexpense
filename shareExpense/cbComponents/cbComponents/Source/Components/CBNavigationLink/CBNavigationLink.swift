//
//  CBNavigationLink.swift
//  shareExpense
//
//  Created by Doruk Kaan Bolu on 2.11.2021.
//

import SwiftUI

public struct CBNavigationLink: View {
    @State public var destination: AnyView?
    @State public var text: String

    public init(destination: AnyView?,
                text:String = "") {
        self.destination = destination
        self.text = text
    }

    public var body: some View {
        NavigationLink(destination: destination) {
            HStack {
                Spacer()
                Text(text)
                    .font(.footnote)
                    .foregroundColor(.secondary)
                
                Spacer()
            }
            .padding(EdgeInsets(top: 8,
                                leading: 0,
                                bottom: 0,
                                trailing: 0))
        }
    }
}


struct CBNavigationLink_Previews: PreviewProvider {
    static var previews: some View {
        CBNavigationLink(destination: AnyView(Text("a")))
    }
}
