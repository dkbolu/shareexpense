//
//  CBButton.swift
//  shareExpense
//
//  Created by Doruk Kaan Bolu on 2.11.2021.
//

import SwiftUI

public struct CBButton: View {
    public var action: () -> Void = {}
    public var text: String
    public var image: Image?
    public var buttonType: CBButtonType
    public var isLoading: Bool


    public init(
        action: @escaping () -> Void = {},
        text: String = "Button",
        image: Image? = nil,
        buttonType: CBButtonType = .primary,
        isLoading: Bool = false
    ) {
        self.action = action
        self.text = text
        self.image = image
        self.buttonType = buttonType
        self.isLoading = isLoading
    }

    public var body: some View {
        ZStack {
            switch buttonType {
            case .primary:
                primaryButtonView()
            case .secondary:
                secondaryButtonView()
            }


            if isLoading {
                HStack {
                    Spacer()
                    Spacer()
                    ProgressView()
                        .progressViewStyle(CircularProgressViewStyle(tint: .white))
                    Spacer()
                }
            }
        }
    }

    @ViewBuilder
    private func primaryButtonView() -> some View {
        HStack {
            Spacer()
            Button(action: action) {
                HStack {
                    Spacer()
                    image
                    Text(text)
                    Spacer()
                }
            }
            .frame(
                idealWidth: .infinity,
                maxWidth: .infinity,
                idealHeight: 44,
                maxHeight: .infinity)
            .background(Color("mediumBlue"))
            .foregroundColor(.white)
            .cornerRadius(20)
            .frame(height: 44)
            Spacer()
        }
    }

    @ViewBuilder
    private func secondaryButtonView() -> some View {
        HStack {
            Spacer()
            Button(action: action) {
                image
                Text(text)
            }
            .frame(
                idealWidth: .infinity,
                maxWidth: .infinity,
                idealHeight: 44,
                maxHeight: .infinity)
            .background(Color.white)
            .foregroundColor(Color("mediumBlue"))
            .cornerRadius(20)
            .overlay(
                RoundedRectangle(cornerRadius: 20.0)
                    .strokeBorder(
                        Color("mediumBlue"),
                        style: StrokeStyle(lineWidth: 1.0)))
            Spacer()
        }
    }
}
struct CBButton_Previews: PreviewProvider {
    static var previews: some View {
        CBButton(buttonType: .secondary)
    }
}

public enum CBButtonType {
    case primary
    case secondary
}
