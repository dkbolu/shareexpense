//
//  CBTextField.swift
//  shareExpense
//
//  Created by Doruk Kaan Bolu on 3.11.2021.
//

import SwiftUI

public struct CBTextField: View {
    public var title: String = ""
    public var text: Binding<String>
    public var textFieldType = CBTextFieldType.plain

    public init(title: String = "",
                text: Binding<String>,
                textFieldType: CBTextFieldType = CBTextFieldType.plain) {
        self.title = title
        self.text = text
        self.textFieldType = textFieldType
    }

    public var body: some View {
        HStack {
            Spacer()
            ZStack {
                RoundedRectangle(cornerRadius: 10)
                    .fill(Color.white)
                    .frame(height: 40)

                view(withTitle: title, text: text)
                    .autocapitalization(.none)
                    .disableAutocorrection(true)
                    .textFieldStyle(PlainTextFieldStyle())
                    .frame(height: 40)
                    .padding(EdgeInsets(top: 0, leading: 4, bottom: 0, trailing: 0))
            }
            Spacer()
        }
    }

    func view(withTitle title: String, text: Binding<String>) -> some View {
        var view: AnyView
        switch textFieldType {
        case .plain:
            view = AnyView(TextField(title, text: text))
        case .secure:
            view = AnyView(SecureField(title, text: text))
        }
        return view
    }
}


struct CBTextField_Previews: PreviewProvider {
    static var previews: some View {
        var string = "asd"
        let bindingString = Binding<String>(
            get: { string },
            set: { string = $0 }
        )
        CBTextField(title: "qwe",
                    text: bindingString,
                    textFieldType: .secure)
    }
}

public enum CBTextFieldType {
    case plain
    case secure
}
