//
//  CBSearchBar.swift
//  cbComponents
//
//  Created by Doruk Kaan Bolu on 7.09.2023.
//  Copyright © 2023 codebehind. All rights reserved.
//

import SwiftUI

public struct CBSearchBar: View {
    @Binding public var searchText: String

    public init(searchText: Binding<String>) {
            self._searchText = searchText
        }

    public var body: some View {
        HStack {
            TextField("Arama yap...", text: $searchText)
                .padding(.horizontal, 20)
                .padding(.vertical, 10)
                .background(Color(.systemGray6))
                .cornerRadius(10)
        }
    }
}

struct CBSearchBar_Previews: PreviewProvider {
    static var previews: some View {
        CBSearchBar(searchText: .constant(""))
    }
}
