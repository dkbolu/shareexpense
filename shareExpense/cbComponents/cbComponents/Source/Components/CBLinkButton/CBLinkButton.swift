//
//  CBLinkButton.swift
//  shareExpense
//
//  Created by Doruk Kaan Bolu on 2.11.2021.
//

import SwiftUI

public struct CBLinkButton: View {
    @State var action: () -> Void = {}
    @State var text: String = "Button"
    public var body: some View {
        HStack {
            Spacer()
            Button(action: action) {
                Text(text)
                    .font(.footnote)
                    .foregroundColor(.secondary)
            }
            Spacer()
        }
        .padding(EdgeInsets(top: 8,
                            leading: 0,
                            bottom: 0,
                            trailing: 0))
    }
}

struct CBLinkButton_Previews: PreviewProvider {
    static var previews: some View {
        CBLinkButton()
    }
}
