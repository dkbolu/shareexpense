// swiftlint:disable all
// swift-format-ignore-file
// swiftformat:disable all
// Generated using tuist — https://github.com/tuist/tuist

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Plist Files

// swiftlint:disable identifier_name line_length number_separator type_body_length
public enum GoogleServiceInfo {
    public static let apiKey: String = "AIzaSyADkZoBPJPiujpzTqSw9_YTNiG-jSX5F_w"
    public static let bundleId: String = "com.codebehind.shareExpense"
    public static let clientId: String = "756599466005-ob47c0c231mstimfqkod374tj132e675.apps.googleusercontent.com"
    public static let databaseUrl: String = "https://bihesap-5cf61.firebaseio.com"
    public static let gcmSenderId: String = "756599466005"
    public static let googleAppId: String = "1:756599466005:ios:669b67d1bd5cc18313c61c"
    public static let isAdsEnabled: Bool = false
    public static let isAnalyticsEnabled: Bool = false
    public static let isAppinviteEnabled: Bool = true
    public static let isGcmEnabled: Bool = true
    public static let isSigninEnabled: Bool = true
    public static let plistVersion: String = "1"
    public static let projectId: String = "bihesap-5cf61"
    public static let reversedClientId: String = "com.googleusercontent.apps.756599466005-ob47c0c231mstimfqkod374tj132e675"
    public static let storageBucket: String = "bihesap-5cf61.appspot.com"
}
// swiftlint:enable identifier_name line_length number_separator type_body_length
// swiftlint:enable all
// swiftformat:enable all
