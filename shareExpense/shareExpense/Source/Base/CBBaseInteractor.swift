//
//  CBBaseInteractor.swift
//  cbFramework
//
//  Created by Doruk Kaan Bolu on 11.02.2023.
//  Copyright © 2023 codebehind. All rights reserved.
//

import Foundation
import cbFramework

public class CBBaseInteractor: CBBaseInteractorProtocol {
    init() {
        print("---Interactor Name: \(Self.self)")
    }
}
