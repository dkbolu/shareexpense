//
//  CBBasePresenter.swift
//  cbFramework
//
//  Created by Doruk Kaan Bolu on 11.02.2023.
//  Copyright © 2023 codebehind. All rights reserved.
//

import Foundation
import cbFramework

public class CBBasePresenter: CBBasePresenterProtocol {
    var baseRouter: CBBaseRouter? = nil
    var baseInteractor: CBBaseInteractor? = nil
    @Published var activeNavigation: BaseRoutes?

    init() {
        print("---Presenter Name: \(Self.self)")
    }
}
