//
//  CBBaseRouter.swift
//  cbFramework
//
//  Created by Doruk Kaan Bolu on 11.02.2023.
//  Copyright © 2023 codebehind. All rights reserved.
//

import Foundation
import cbFramework

public class CBBaseRouter: CBBaseRouterProtocol {
    
}

enum BaseRoutes {
    case welcome(_: WelcomeRoutes)
    case home(_: HomeRoutes)
    case signup(_: SignUpRoutes)
    case login(_: LoginRoutes)
}

extension BaseRoutes: Hashable {

    func hash(into hasher: inout Hasher) {

        switch self {
        case .welcome(let value):
            hasher.combine(value) // combine with associated value, if it's not `Hashable` map it to some `Hashable` type and then combine result
        case .home(let value):
            hasher.combine(value) // combine with associated value, if it's not `Hashable` map it to some `Hashable` type and then combine result
        case .signup(let value):
            hasher.combine(value) // combine with associated value, if it's not `Hashable` map it to some `Hashable` type and then combine result
        case .login(let value):
            hasher.combine(value) // combine with associated value, if it's not `Hashable` map it to some `Hashable` type and then combine result
        }
    }
}
