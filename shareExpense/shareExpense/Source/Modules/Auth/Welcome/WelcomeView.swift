//
//  WelcomeView.swift
//  shareExpense
//
//  Created by Doruk Kaan Bolu on 2.11.2021.
//

import SwiftUI
import cbComponents

struct WelcomeView: View {
    @ObservedObject var presenter: WelcomePresenter

    var body: some View {
        ZStack {
            Color("iceBlue")
                .edgesIgnoringSafeArea(.all)
            VStack {
                Spacer()
                Text("Hello, world!\nPlease Sign In")
                    .padding()
                Spacer()
                CBButton(action: {
                    presenter.activeNavigation = .welcome(.login)
                }, text: "Sign In")
                CBNavigationLink(destination: presenter.navigate(.signup),
                                 text: "If you don't have an account, please press here to Sign Up")
                Spacer()
                NavigationLink(
                    destination: presenter.navigate(.home),
                    tag: .welcome(.home),
                    selection: $presenter.activeNavigation) {
                        EmptyView()
                    }
                NavigationLink(
                    destination: presenter.navigate(.signup),
                    tag: .welcome(.signup),
                    selection: $presenter.activeNavigation) {
                        EmptyView()
                    }
                NavigationLink(
                    destination: presenter.navigate(.login),
                    tag: .welcome(.login),
                    selection: $presenter.activeNavigation) {
                        EmptyView()
                    }
            }
        }
    }
}

struct WelcomeView_Previews: PreviewProvider {
    static var previews: some View {
        let presenter = WelcomePresenter()
        WelcomeView(presenter: presenter)
    }
}
