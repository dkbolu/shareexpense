//
//  WelcomeRouter.swift
//  shareExpense
//
//  Created by Doruk Kaan Bolu on 2.11.2021.
//

import Foundation

class WelcomeRouter: CBBaseRouter {

    func navigate(_ route: WelcomeRoutes) -> CBAnyView {
        switch route {
        case .signup:
            return CBAnyView(SignUpView(presenter: SignUpPresenter()))
        case .home:
            return CBAnyView(ContainerView(presenter: ContainerPresenter(), selection: .home))
        case .login:
            return CBAnyView(LoginView(presenter: LoginPresenter()))
        }
    }
}

enum WelcomeRoutes {
    case signup
    case home
    case login
}
