//
//  WelcomePresenter.swift
//  shareExpense
//
//  Created by Doruk Kaan Bolu on 2.11.2021.
//  Copyright © 2022 codebehind. All rights reserved.
//

import Foundation

//protocol WelcomePresenterProtocol: CBBasePresenter {
//     requirements
//}

class WelcomePresenter: CBBasePresenter, ObservableObject {
    private var router: WelcomeRouter? {
        get { super.baseRouter as? WelcomeRouter}
        set { super.baseRouter = newValue}
    }
    private var interactor: WelcomeInteractor? {
        get { super.baseInteractor as? WelcomeInteractor}
        set { super.baseInteractor = newValue}
    }

    override init() {
        super.init()
        router = WelcomeRouter()
        interactor = WelcomeInteractor()
        interactor?.checkUserIsLoggedIn(completion: { isLoggedIn in
            self.activeNavigation = isLoggedIn ? .welcome(.home) : nil
        })
    }

    func navigate(_ route: WelcomeRoutes) -> CBAnyView? {
        return router?.navigate(route)
    }
}
