//
//  SignUpView.swift
//  shareExpense
//
//  Created by Doruk Kaan Bolu on 2.11.2021.
//

import SwiftUI
import cbComponents

struct SignUpView: View {
    @ObservedObject var presenter: SignUpPresenter
    @State var email: String = ""
    @State var name: String = ""
    @State var password: String = ""
    var body: some View {
        ZStack {
            Color("iceBlue")
                .edgesIgnoringSafeArea(.all)

            NavigationLink(destination: presenter.navigate(.home), tag: .signup(.home), selection: $presenter.activeNavigation) {
                EmptyView()
            }
            NavigationLink(destination: presenter.navigate(.login), tag: .signup(.login), selection: $presenter.activeNavigation) {
                EmptyView()
            }
            VStack {
                Spacer()
                Spacer()
                Spacer()
                Text("Create Account").font(.title)
                Group {
                    CBTextField(title: "name", text: $name)
                    CBTextField(title: "email", text: $email)
                    CBTextField(title: "password",
                                text: $password,
                                textFieldType: .secure)
                }
                Spacer()
                Spacer()
                Group{
                    CBButton(action: {
                        presenter.signUpButtonTap(name: name, email: email, password: password)
                    }, text: "Sign Up")
                    HStack {
                        Spacer()
                        VStack{
                            Divider()
                        }
                        Text("or").font(.footnote).foregroundColor(.secondary)
                        VStack{
                            Divider()
                        }
                        Spacer()

                    }
                    CBNavigationLink(destination: presenter.navigate(.login),
                                     text: "If you have an account, please press here to Sign In")
                }
                Spacer()
                Spacer()
            }
        }
    }
}

struct SignUpView_Previews: PreviewProvider {
    static var previews: some View {
        let presenter = SignUpPresenter()
        SignUpView(presenter: presenter)
    }
}
