//
//  SignUpRouter.swift
//  ShareExpense
//
//  Created by Doruk Kaan Bolu on 12.10.2022.
//  Copyright © 2022 codebehind. All rights reserved.
//

import Foundation

class SignUpRouter: CBBaseRouter {
    func navigate(_ route: SignUpRoutes) -> CBAnyView {
        switch route {
        case .home:
            return CBAnyView(ContainerView(presenter: ContainerPresenter(), selection: .home))
        case .login:
            return CBAnyView(LoginView(presenter: LoginPresenter()))
        }
    }
}

enum SignUpRoutes {
    case home
    case login
}
