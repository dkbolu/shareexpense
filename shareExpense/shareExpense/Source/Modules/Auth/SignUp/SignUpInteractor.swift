//
//  SignUpInteractor.swift
//  ShareExpense
//
//  Created by Doruk Kaan Bolu on 11.02.2023.
//  Copyright © 2023 codebehind. All rights reserved.
//

import Foundation
import FirebaseFirestore

class SignUpInteractor: CBBaseInteractor, FirebaseAuthHelper, ObservableObject {
    var authStateListener: AuthStateListener?
    deinit {
        releaseListener()
    }
    
    func signUp(email: String, password: String, completion: @escaping (String?, Error?) -> Void) {
        firebaseSignUp(email: email, password: password, completion: completion)
    }

    func checkUserIsLoggedIn(completion: @escaping (Bool) -> Void) {
        firebaseCheckUserIsLoggedIn { uid, success  in
            if let uid = uid {
                self.fetchUser(userId: uid) { user in
                    ApplicationContext.shared.user = user
                    completion(success)
                }
            }
        }
    }

    func createAppUser(userId: String, name: String, email: String, completion: @escaping (Bool) -> Void)  {

        let dataToSave = [
            "name": name,
            "email": email,
            "userId": userId,
            "createdTimestamp": FieldValue.serverTimestamp(),
            "isActive": true
        ] as [String : Any]
        let db = Firestore.firestore()
        let collectionRef = db.collection("appUser")


        collectionRef.addDocument(data: dataToSave) { error in
            if let error = error {
                print("Veri kaydetme hatası: \(error.localizedDescription)")
                completion(false)
            } else {
                print("Veri başarıyla Firestore'e kaydedildi.")
                completion(true)
            }
        }
    }

    func fetchUser(userId: String, completion: @escaping (UserModel?) -> Void) {
        let db = Firestore.firestore()
        let collectionRef = db.collection("appUser")

        collectionRef.whereField("userId", isEqualTo: userId).getDocuments { (querySnapshot, error) in
            if let error = error {
                print("Veri çekme hatası: \(error.localizedDescription)")
                completion(nil)
            } else if let documents = querySnapshot?.documents, !documents.isEmpty {
                let document = documents[0]
                let user = UserModel()
                user.userId = document["userId"] as? String ?? ""
                user.userName = document["name"] as? String ?? ""
                user.email = document["email"] as? String ?? ""
                if let timestamp = document["createdTimestamp"] as? Timestamp {
                    user.createdDate = timestamp.dateValue()
                }
                user.isActive = document["isActive"] as? Bool ?? true
                completion(user)
            } else {
                completion(nil)
            }
        }
    }

}
