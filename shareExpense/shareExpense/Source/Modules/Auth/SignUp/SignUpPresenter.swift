//
//  SignUpPresenter.swift
//  ShareExpense
//
//  Created by Doruk Kaan Bolu on 10.10.2022.
//  Copyright © 2022 codebehind. All rights reserved.
//

import Foundation

class SignUpPresenter: CBBasePresenter, ObservableObject {
    private var router: SignUpRouter? {
        get { super.baseRouter as? SignUpRouter}
        set { super.baseRouter = newValue}
    }
    private var interactor: SignUpInteractor? {
        get { super.baseInteractor as? SignUpInteractor}
        set { super.baseInteractor = newValue}
    }

    override init() {
        super.init()
        router = SignUpRouter()
        interactor = SignUpInteractor()
        interactor?.checkUserIsLoggedIn(completion: { isLoggedIn in
            self.activeNavigation = isLoggedIn ? .signup(.home) : nil
        })
    }

    func signUpButtonTap(name: String, email: String, password: String) {
        guard validate(name: name, email: email, password: password) else {
            return
        }
        signUp(name: name, email: email, password: password)
    }

    private func validate(name: String, email: String, password: String) -> Bool {
        return !name.isEmpty && !email.isEmpty && !password.isEmpty
    }
    
    private func signUp(name: String, email: String, password: String) {
        interactor?.signUp(email: email, password: password) { userId, error in
            guard let userId = userId else {
                return
            }

            self.interactor?.createAppUser(userId: userId, name: name, email: email, completion: { success in
                if success {
                    self.activeNavigation = success ? .signup(.home) : nil
                }
            })
        }
    }

    func navigate(_ route: SignUpRoutes) -> CBAnyView? {
        return router?.navigate(route)
    }
}
