//
//  LoginRouter.swift
//  ShareExpense
//
//  Created by Doruk Kaan Bolu on 12.10.2022.
//  Copyright © 2022 codebehind. All rights reserved.
//

import Foundation

class LoginRouter: CBBaseRouter {
    func navigate(_ route: LoginRoutes) -> CBAnyView {
        switch route {
        case .signup:
            return CBAnyView(SignUpView(presenter: SignUpPresenter()))
        case .home:
            return CBAnyView(ContainerView(presenter: ContainerPresenter(), selection: .home))
        }
    }
}

enum LoginRoutes {
    case home
    case signup
}
