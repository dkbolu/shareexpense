//
//  LoginPresenter.swift
//  ShareExpense
//
//  Created by Doruk Kaan Bolu on 12.10.2022.
//  Copyright © 2022 codebehind. All rights reserved.
//

import Foundation

class LoginPresenter: CBBasePresenter, ObservableObject {
    private var router: LoginRouter? {
        get { super.baseRouter as? LoginRouter}
        set { super.baseRouter = newValue}
    }
    private var interactor: LoginInteractor? {
        get { super.baseInteractor as? LoginInteractor}
        set { super.baseInteractor = newValue}
    }

    override init() {
        super.init()
        router = LoginRouter()
        interactor = LoginInteractor()
    }

    func loginButtonTap(email: String, password: String) {
        guard validate(email: email, password: password) else {
            return
        }
        login(email: email, password: password)
    }

    private func validate(email: String, password: String) -> Bool {
        return !email.isEmpty && !password.isEmpty
    }

    private func login(email: String, password: String) {
        interactor?.login(email: email, password: password) { isLoggedIn in
            self.activeNavigation = isLoggedIn ? .login(.home) : nil
        }
    }

    func navigate(_ route: LoginRoutes) -> CBAnyView? {
        return router?.navigate(route)
    }
}
