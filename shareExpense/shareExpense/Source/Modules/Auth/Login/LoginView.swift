//
//  LoginView.swift
//  ShareExpense
//
//  Created by Doruk Kaan Bolu on 12.10.2022.
//  Copyright © 2022 codebehind. All rights reserved.
//

import SwiftUI
import cbComponents

struct LoginView: View {
    @ObservedObject var presenter: LoginPresenter
    @State var email: String = ""
    @State var password: String = ""
    var body: some View {
        ZStack {
            Color("iceBlue")
                .edgesIgnoringSafeArea(.all)
            NavigationLink(destination: presenter.navigate(.home), tag: .login(.home), selection: $presenter.activeNavigation) {
                EmptyView()
            }
            VStack {
                Spacer()
                Spacer()
                Spacer()
                Text("Sign In").font(.title)
                Group {
                    CBTextField(title: "email", text: $email)
                    CBTextField(title: "password",
                                text: $password,
                                textFieldType: .secure)
                }
                Spacer()
                Spacer()
                
                Group{
                    CBButton(action: {
                        presenter.loginButtonTap(email: email, password: password)
                    }, text: "Sign In")
                    HStack {
                        Spacer()
                        VStack{
                            Divider()
                        }
                        Text("or").font(.footnote).foregroundColor(.secondary)
                        VStack{
                            Divider()
                        }
                        Spacer()
                        
                    }
                    CBNavigationLink(destination: presenter.navigate(.signup),
                                     text: "If you don't have an account, please pres here to Sign Up")
                }
                Spacer()
                Spacer()
            }
        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView(presenter: LoginPresenter())
    }
}
