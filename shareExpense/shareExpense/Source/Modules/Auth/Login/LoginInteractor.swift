//
//  LoginInteractor.swift
//  ShareExpense
//
//  Created by Doruk Kaan Bolu on 13.10.2022.
//  Copyright © 2022 codebehind. All rights reserved.
//

import Foundation

class LoginInteractor: CBBaseInteractor,FirebaseAuthHelper, ObservableObject {
    var authStateListener: AuthStateListener?
    deinit {
        releaseListener()
    }
    
    func login(email: String, password: String,  completion: @escaping (Bool) -> Void) {
        firebaseLogin(email: email, password: password, completion: completion)
    }
}
