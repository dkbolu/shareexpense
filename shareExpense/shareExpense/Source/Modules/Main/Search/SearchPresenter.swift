//
//  SearchPresenter.swift
//  ShareExpense
//
//  Created by Doruk Kaan Bolu on 7.09.2023.
//  Copyright © 2023 codebehind. All rights reserved.
//

import UIKit

class SearchPresenter: CBBasePresenter, ObservableObject {
    private var interactor: SearchInteractor? {
        get { super.baseInteractor as? SearchInteractor}
        set { super.baseInteractor = newValue}
    }

    @Published var userList: [UserModel] = []

    override init() {
        super.init()
        //router = WelcomeRouter()
        interactor = SearchInteractor()
    }

    func searchUsers(text: String) {
        interactor?.fetchUsers(searchText: text, completion: { users in
            guard let users = users else { return }
            self.userList = users
        })
    }
}
