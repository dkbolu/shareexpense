//
//  SearchInteractor.swift
//  ShareExpense
//
//  Created by Doruk Kaan Bolu on 7.09.2023.
//  Copyright © 2023 codebehind. All rights reserved.
//

import FirebaseFirestore

class SearchInteractor: CBBaseInteractor {
    func fetchUsers(searchText: String, completion: @escaping ([UserModel]?) -> Void) {
        guard let userId = ApplicationContext.shared.user?.userId else {
            return
        }

        let db = Firestore.firestore()
        let collectionRef = db.collection("appUser")

        collectionRef.whereField("userId", isNotEqualTo: userId)
            .whereField("isActive", isEqualTo: true)

        if !searchText.isEmpty {
            collectionRef.whereField("name", isEqualTo: searchText)
        }

        collectionRef.getDocuments { (querySnapshot, error) in
            if let error = error {
                print("Veri çekme hatası: \(error.localizedDescription)")
                completion(nil)
                return
            } else if let documents = querySnapshot?.documents {
                let userList = documents.compactMap { document -> UserModel? in
                    let user = UserModel()
                    user.userId = document["userId"] as? String ?? ""
                    user.userName = document["name"] as? String ?? ""
                    user.email = document["email"] as? String ?? ""
                    if let timestamp = document["createdTimestamp"] as? Timestamp {
                        user.createdDate = timestamp.dateValue()
                    }
                    user.isActive = document["isActive"] as? Bool ?? true
                    return user

                }
                completion(userList)
            } else {
                completion([])
            }
        }
    }
}
