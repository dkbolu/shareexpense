//
//  SearchView.swift
//  ShareExpense
//
//  Created by Doruk Kaan Bolu on 7.09.2023.
//  Copyright © 2023 codebehind. All rights reserved.
//

import SwiftUI
import cbComponents

struct SearchView: View {
    @StateObject var presenter: SearchPresenter
    @State private var searchText = ""

    var body: some View {
        ZStack {
            Color("iceBlue")
                .edgesIgnoringSafeArea(.all)
            VStack {
                CBSearchBar(searchText: $searchText)
                    .padding(.horizontal)

                List {
                    ForEach(presenter.userList.filter {
                        searchText.isEmpty || $0.userName.localizedCaseInsensitiveContains(searchText)
                    }, id: \.userId) { user in
                        Text(user.userName)
                            .padding(.vertical, 5)
                    }
                }
                .cornerRadius(30)
                .listStyle(InsetListStyle())
                .padding(.horizontal, 20)
            }.onAppear {
                presenter.searchUsers(text: "")
            }
        }
    }
}

struct SearchView_Previews: PreviewProvider {
    static var previews: some View {
        let presenter = SearchPresenter()
        SearchView(presenter: presenter)
    }
}
