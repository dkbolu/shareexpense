//
//  ContainerRouter.swift
//  ShareExpense
//
//  Created by Doruk Kaan Bolu on 12.02.2023.
//  Copyright © 2023 codebehind. All rights reserved.
//

import Foundation

class ContainerRouter: CBBaseRouter {
    func navigate(_ route: ContainerRoutes) -> CBAnyView {
        switch route {
        case .home:
            return CBAnyView(HomeView(presenter: HomePresenter()))
        case .groups:
            return CBAnyView(GroupView(presenter: GroupPresenter()))
        case .search:
            return CBAnyView(SearchView(presenter: SearchPresenter()))
        }
    }
}

enum ContainerRoutes {
    case home
    case groups
    case search
}
