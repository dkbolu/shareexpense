//
//  ContainerView.swift
//  ShareExpense
//
//  Created by Doruk Kaan Bolu on 12.02.2023.
//  Copyright © 2023 codebehind. All rights reserved.
//

import SwiftUI

struct ContainerView: View {
    @ObservedObject var presenter: ContainerPresenter
    @State var selection = ContainerRoutes.home
    var body: some View {
        ZStack {
            //Color("iceBlue")
            //    .edgesIgnoringSafeArea(.all)
            VStack {
                TabView(selection: $selection) {
                    presenter.navigate(.home)
                        .tabItem {
                            Label("Home", systemImage: "homekit")
                        }
                        .tag(ContainerRoutes.home)

                    presenter.navigate(.search)
                        .tabItem {
                            Label("Search", systemImage: "magnifyingglass")
                        }
                        .tag(ContainerRoutes.search)

                    presenter.navigate(.groups)
                        .tabItem {
                            Label("Groups", systemImage: "rectangle.3.group")
                        }
                        .tag(ContainerRoutes.groups)
                }
            }
        }
    }
}

struct ContainerView_Previews: PreviewProvider {
    static var previews: some View {
        ContainerView(presenter: ContainerPresenter())
    }
}
