//
//  ContainerPresenter.swift
//  ShareExpense
//
//  Created by Doruk Kaan Bolu on 12.02.2023.
//  Copyright © 2023 codebehind. All rights reserved.
//

import Foundation

class ContainerPresenter: CBBasePresenter, ObservableObject {
    private var router: ContainerRouter? {
        get { super.baseRouter as? ContainerRouter}
        set { super.baseRouter = newValue}
    }
    /*private var interactor: LoginInteractor? {
        get { super.baseInteractor as? LoginInteractor}
        set { super.baseInteractor = newValue}
    }*/

    override init() {
        super.init()
        router = ContainerRouter()
    }

    func navigate(_ route: ContainerRoutes) -> CBAnyView? {
        return router?.navigate(route)
    }
}
