//
//  HomePresenter.swift
//  ShareExpense
//
//  Created by Doruk Kaan Bolu on 12.10.2022.
//  Copyright © 2022 codebehind. All rights reserved.
//

import Foundation

class HomePresenter: CBBasePresenter, ObservableObject {
    private var router: HomeRouter? {
        get { super.baseRouter as? HomeRouter}
        set { super.baseRouter = newValue}
    }
    private var interactor: HomeInteractor? {
        get { super.baseInteractor as? HomeInteractor}
        set { super.baseInteractor = newValue}
    }

    override init() {
        super.init()
        router = HomeRouter()
        interactor = HomeInteractor()
        interactor?.checkUserIsLoggedIn(completion: { isLoggedIn in
            self.activeNavigation = isLoggedIn ? nil : .home(.login)
        })
    }

    func navigate(_ route: HomeRoutes) -> CBAnyView? {
        return router?.navigate(route)
    }
}
