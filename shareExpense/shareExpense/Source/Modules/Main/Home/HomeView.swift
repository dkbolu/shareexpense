//
//  HomeView.swift
//  ShareExpense
//
//  Created by Doruk Kaan Bolu on 10.10.2022.
//  Copyright © 2022 codebehind. All rights reserved.
//

import SwiftUI
import cbComponents
import FirebaseAuth

struct HomeView: View {
    @ObservedObject var presenter: HomePresenter
    var body: some View {
        ZStack {
            VStack {
                Text("Home")
                CBButton(action: {
                    do {
                        try Auth.auth().signOut()
                    } catch {
                        
                    }
                }, text: "Logout", image: nil, buttonType: .primary)
                
                NavigationLink(destination: presenter.navigate(.login), tag: .home(.login), selection: $presenter.activeNavigation) {
                    EmptyView()
                }
            }
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        let presenter = HomePresenter()
        HomeView(presenter: presenter)
    }
}
