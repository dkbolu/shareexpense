//
//  HomeRouter.swift
//  ShareExpense
//
//  Created by Doruk Kaan Bolu on 12.10.2022.
//  Copyright © 2022 codebehind. All rights reserved.
//

import Foundation

class HomeRouter: CBBaseRouter {
    func navigate(_ route: HomeRoutes) -> CBAnyView {
        switch route {
        case .login:
            return CBAnyView(LoginView(presenter: LoginPresenter()))
        }
    }
}

enum HomeRoutes {
    case login
}
