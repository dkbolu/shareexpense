//
//  HomeInteractor.swift
//  ShareExpense
//
//  Created by Doruk Kaan Bolu on 7.09.2023.
//  Copyright © 2023 codebehind. All rights reserved.
//

import Foundation
import FirebaseFirestore

class HomeInteractor: CBBaseInteractor, FirebaseAuthHelper, ObservableObject {
    var authStateListener: AuthStateListener?
    deinit {
        releaseListener()
    }

    func checkUserIsLoggedIn(completion: @escaping (Bool) -> Void) {
        firebaseCheckUserIsLoggedIn { uid, success  in
            if let uid = uid {
                self.fetchUser(userId: uid) { user in
                    ApplicationContext.shared.user = user
                    completion(success)
                }
            }
        }
    }

    func fetchUser(userId: String, completion: @escaping (UserModel?) -> Void) {
        let db = Firestore.firestore()
        let collectionRef = db.collection("appUser")

        collectionRef.whereField("userId", isEqualTo: userId).getDocuments { (querySnapshot, error) in
            if let error = error {
                print("Veri çekme hatası: \(error.localizedDescription)")
                completion(nil)
            } else if let documents = querySnapshot?.documents, !documents.isEmpty {
                let document = documents[0]
                let user = UserModel()
                user.userId = document["userId"] as? String ?? ""
                user.userName = document["name"] as? String ?? ""
                user.email = document["email"] as? String ?? ""
                if let timestamp = document["createdTimestamp"] as? Timestamp {
                    user.createdDate = timestamp.dateValue()
                }
                user.isActive = document["isActive"] as? Bool ?? true
                completion(user)
            } else {
                completion(nil)
            }
        }
    }
}
