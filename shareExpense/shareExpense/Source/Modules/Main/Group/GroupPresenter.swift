//
//  GroupPresenter.swift
//  ShareExpense
//
//  Created by Doruk Kaan Bolu on 7.09.2023.
//  Copyright © 2023 codebehind. All rights reserved.
//

import Foundation

class GroupPresenter: CBBasePresenter, ObservableObject {
    /*private var router: WelcomeRouter? {
     get { super.baseRouter as? WelcomeRouter}
     set { super.baseRouter = newValue}
     }*/
    private var interactor: GroupInteractor? {
        get { super.baseInteractor as? GroupInteractor}
        set { super.baseInteractor = newValue}
    }

    @Published var groupList: [GroupModel] = []
    @Published var isLoading = false

    override init() {
        super.init()
        //router = WelcomeRouter()
        interactor = GroupInteractor()
    }

    //func navigate(_ route: WelcomeRoutes) -> CBAnyView? {
    //    return router?.navigate(route)
    //}

    func createGroup(name: String) {
        isLoading = true
        interactor?.createGroup(name: name) { [weak self] in
            self?.getGroups()

        }
    }

    func getGroups() {
        interactor?.fetchGroups { [weak self] groups in
            if let groups = groups {
                DispatchQueue.main.async {
                    self?.groupList = groups.sorted(by: { $0.createdDate ?? Date() > $1.createdDate ?? Date() })
                    self?.isLoading = false
                }
            }
        }
    }
}
