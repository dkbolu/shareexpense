//
//  GroupView.swift
//  ShareExpense
//
//  Created by Doruk Kaan Bolu on 7.09.2023.
//  Copyright © 2023 codebehind. All rights reserved.
//

import SwiftUI
import cbComponents
import cbFramework

struct GroupView: View {
    @State private var inputValue = ""
    @StateObject var presenter: GroupPresenter
    
    var body: some View {
        ZStack {
            Color("iceBlue")
                .edgesIgnoringSafeArea(.all)
            
            if presenter.groupList.isEmpty {
                createGroupView()
            } else {
                groupsListView()
            }
        }
        .onAppear(perform: presenter.getGroups)
    }
    
    @ViewBuilder
    private func createGroupView() -> some View {
        VStack {
            Group {
                Spacer()
                Spacer()
                Spacer()
            }
            Text("Create Group")
                .font(.title)
            
            CBTextField(title: "Group Name", text: $inputValue)
                .padding()
            
            Group {
                Spacer()
                Spacer()
            }
            
            CBButton(action: {
                presenter.createGroup(name: inputValue)
                inputValue = ""
            },
                     text: "Create",
                     isLoading: presenter.isLoading)
            .padding()
            .disabled(presenter.isLoading)
            
            Group {
                Spacer()
                Spacer()
                Spacer()
            }
        }
    }
    
    @ViewBuilder
    private func groupsListView() -> some View {
        VStack {
            Spacer()
            Text("Groups")
                .font(.title)
            
            List(presenter.groupList, id: \.uuid) { item in
                VStack(alignment: .leading) {
                    Text(item.name)
                        .font(.headline)
                    Text(item.createdDate?.toString(dateFormat: "dd.MM.yyyy") ?? "")
                        .font(.footnote)

                }
                .padding(.vertical, 5)
            }
            .cornerRadius(30)
            .listStyle(InsetListStyle())
            .padding(.horizontal, 20)
            
            Group {
                Spacer()
                Spacer()
            }
            
            VStack {
                HStack {
                    Text("Create New Group")
                        .font(.subheadline)
                        .padding()
                    
                    CBTextField(title: "Group Name", text: $inputValue)
                        .padding()
                }
                
                CBButton(action: {
                    presenter.createGroup(name: inputValue)
                    inputValue = ""
                },
                         text: "Create",
                         isLoading: presenter.isLoading)
                .padding()
                .disabled(presenter.isLoading)
            }
            
            Group {
                Spacer()
                Spacer()
                Spacer()
            }
        }
    }
}

struct GroupView_Previews: PreviewProvider {
    static var previews: some View {
        let presenter = GroupPresenter()
        presenter.groupList = [GroupModel(name: "asd", createdDate: Date()),
                               GroupModel(name: "asd", createdDate: Date()),
                               GroupModel(name: "asd", createdDate: Date())]
        //presenter.groupList = []
        return GroupView(presenter: presenter)
    }
}
