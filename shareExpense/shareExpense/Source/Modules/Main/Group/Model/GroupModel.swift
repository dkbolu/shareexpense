//
//  GroupModel.swift
//  ShareExpense
//
//  Created by Doruk Kaan Bolu on 7.09.2023.
//  Copyright © 2023 codebehind. All rights reserved.
//

import Foundation

struct GroupModel {
    var uuid = ""
    var name = ""
    var creatorUserId = ""
    var createdDate: Date?
}
