//
//  GroupInteractor.swift
//  ShareExpense
//
//  Created by Doruk Kaan Bolu on 7.09.2023.
//  Copyright © 2023 codebehind. All rights reserved.
//

import FirebaseFirestore

class GroupInteractor: CBBaseInteractor {
    func createGroup(name: String, completion: @escaping () -> Void) {
        guard let userId = ApplicationContext.shared.user?.userId else {
            return
        }
        let dataToSave = [
            "groupUUID": UUID().uuidString,
            "groupName": name,
            "userId": userId,
            "createdTimestamp": FieldValue.serverTimestamp()
        ] as [String : Any]

        let db = Firestore.firestore()
        let collectionRef = db.collection("group")


        collectionRef.addDocument(data: dataToSave) { error in
            if let error = error {
                print("Veri kaydetme hatası: \(error.localizedDescription)")
            } else {
                print("Veri başarıyla Firestore'e kaydedildi.")
                completion()
            }
        }
    }

    func fetchGroups(completion: @escaping ([GroupModel]?) -> Void) {
        guard let userId = ApplicationContext.shared.user?.userId else {
            return
        }
        let db = Firestore.firestore()
        let collectionRef = db.collection("group")

        collectionRef.whereField("userId", isEqualTo: userId).getDocuments { (querySnapshot, error) in
            if let error = error {
                error.localizedDescription.po()
                return
            } else if let documents = querySnapshot?.documents {
                let groupList = documents.compactMap { document -> GroupModel? in
                    guard let uuid = document["groupUUID"] as? String else {
                        return nil
                    }
                    let name = document["groupName"] as? String ?? ""
                    let creatorUserId = document["userId"] as? String ?? ""
                    let timestamp = document["createdTimestamp"] as? Timestamp
                    let date = timestamp?.dateValue()
                    return GroupModel(uuid: uuid, name: name, creatorUserId: creatorUserId, createdDate: date)
                }
                completion(groupList)
            }
        }
    }
}
