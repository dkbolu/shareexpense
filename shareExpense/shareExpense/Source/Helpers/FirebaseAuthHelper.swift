//
//  FirebaseAuthHelper.swift
//  ShareExpense
//
//  Created by Doruk Kaan Bolu on 10.10.2022.
//  Copyright © 2022 codebehind. All rights reserved.
//

import Foundation
import FirebaseAuth

protocol FirebaseAuthHelper: AnyObject {
    typealias AuthStateListener = AuthStateDidChangeListenerHandle
    var authStateListener :AuthStateListener? { get set }
    func firebaseCheckUserIsLoggedIn(completion: @escaping (String?, Bool) -> Void)
    func releaseListener()
    func firebaseLogin(email: String, password: String,  completion: @escaping (Bool) -> Void)
    func firebaseSignUp(email: String, password: String, completion: @escaping (String?, Error?) -> Void)
}

extension FirebaseAuthHelper {
    func firebaseCheckUserIsLoggedIn(completion: @escaping (String?, Bool) -> Void) {
        authStateListener = Auth.auth().addStateDidChangeListener({ auth, user in
            if let uid = user?.uid, !uid.isEmpty {
                completion(uid, true)
            } else {
                completion(nil, false)
            }
        })
    }

    func releaseListener() {
        if let authStateListener = authStateListener {
            Auth.auth().removeStateDidChangeListener(authStateListener)
        }
    }

    func firebaseLogin(email: String, password: String,  completion: @escaping (Bool) -> Void) {
        Auth.auth().signIn(withEmail: email, password: password) { result, error in
            guard let result = result else {
                "sign in error".po()
                if let error = error {
                    error.localizedDescription.po()
                }
                completion(false)
                return
            }
            completion(true)
            result.debugDescription.po()
        }
    }

    func firebaseSignUp(email: String, password: String, completion: @escaping (String?, Error?) -> Void) {
        Auth.auth().createUser(withEmail: email, password: password) { result, error in
            guard let result = result else {
                "sign up error".po()
                if let error = error {
                    error.localizedDescription.po()
                }
                completion(nil, error)
                return
            }
            completion(result.user.uid, nil)
            result.debugDescription.po()
        }
    }
}
