//
//  ContentView.swift
//  shareExpense
//
//  Created by Doruk Kaan Bolu on 2.11.2021.
//

import SwiftUI

typealias CBAnyView = AnyView

struct ContentView: View {
    var body: some View {
        NavigationView {
            WelcomeView(presenter: WelcomePresenter())
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
