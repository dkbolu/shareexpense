//
//  ApplicationContext.swift
//  cbframework
//
//  Created by Doruk Kaan Bolu on 1.11.2021.
//

import Foundation
import cbFramework

class ApplicationContext: ApplicationContextProtocol {

    static let shared = ApplicationContext()
    //var blockerView: UIView?

    var langId: Language {
        get {
            let langId = UserDefaultsManager.shared.readString(key: "langId")
            return Language(rawValue: langId) ?? .turkish
        } set {
            UserDefaultsManager.shared.writeString(string: newValue.rawValue, key: "langId")
        }
    }

    var debugMod: Bool
    let appName = "SharedExpense"
    let appVersion: String
    let appBundleVersion: String
    var serviceEnvironment: ServiceEnvironment

    var user: UserModel?
    var sessionId = ""

    init() {
        appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        appBundleVersion = Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? ""
        sessionId = UUID().uuidString

        #if DEBUG
        debugMod = true
        serviceEnvironment = .dev
        #else
        debugMod = false
        serviceEnvironment = .test
        #endif
    }

}

enum ServiceEnvironment: String, CaseIterable {
    case local
    case dev
    case test
    case preProduction
    case prod
}
