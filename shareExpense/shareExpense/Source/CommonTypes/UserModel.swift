//
//  UserModel.swift
//  ShareExpense
//
//  Created by Doruk Kaan Bolu on 7.09.2023.
//  Copyright © 2023 codebehind. All rights reserved.
//

import Foundation

class UserModel {
    var userId = ""
    var userName = ""
    var email = ""
    var createdDate: Date?
    var isActive = true

}
